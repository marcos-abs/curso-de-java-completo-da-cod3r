/**
 * CalculoTeste
 * @version 1.0
 * @author Marcos Antônio Barbosa de Souza
 * @created 2024-04-08
 * @updated on 2024-04-08
 * @see https://www.udemy.com/course/fundamentos-de-programacao-com-java/
 */

package lambdas;

public class CalculoTeste1 {
    public static void main(String[] args) {
        Calculo calculo = new Somar();
        System.out.println(calculo.executar(2, 3));

        calculo = new Multiplicar();
        System.out.println(calculo.executar(2, 3));
    }
}
