package lambdas;

import java.util.Locale;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class Desafio {
    public static void main(String[] args) {
        // 1. A partir do produto, calcular o preço real (com desconto)
        // 2. Imposto Municipal: >= 2500 (8,5%) / < 2500 (Isento)
        // 3. Frete: >= 3000 (100) / < 3000 (50)
        // 4. Arredondar: Deixar duas casas decimais
        // 5. Formatar: R$1234,56

        Produto p = new Produto("iPad", 3235.89, 0.13);

        Function<Produto, Double> desconto = produto -> produto.preco * (1 - produto.desconto);
        UnaryOperator<Double> imposto = preco -> preco >= 2500 ? preco * 1.085 : preco;
        UnaryOperator<Double> frete = preco -> preco >= 3000 ? preco + 100 : preco + 50;
        UnaryOperator<Double> arredondar = preco -> (double) Math.round(preco * 100) / 100;
        Function<Double, String> formatar = preco -> (String.format(new Locale("pt-br"), "R$%,.2f", preco)).replace(",","#").replace(".",",").replace("#",".");

        String precoFormatado = desconto
                .andThen(imposto)
                .andThen(frete)
                .andThen(arredondar)
                .andThen(formatar)
                .apply(p);

        System.out.println("O preço final é " + precoFormatado);
    }
}
