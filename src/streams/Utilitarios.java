package streams;

import java.util.function.UnaryOperator;

public interface Utilitarios {
    public static String maiuscula (String n) {
        return n.toUpperCase();
    }
    public final static UnaryOperator<String> primeiraLetra = n -> n.charAt(0) + "";
    public static String grito (String n) {
      return n + "!!! ";
    }
}
