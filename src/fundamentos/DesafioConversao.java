/*
 * *****
 * File: DesafioConversao.java
 * Project: Curso de Java Completo da Cod3r
 * Path: /home/marcos/.path-shortcuts/curso-java-cod3r
 * File Created: Monday, 05 June 2023 15:43:36
 * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 05 June 2023 16:40:52
 * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Copyright (c) 2023 All rights reserved, Marcos Antônio Barbosa de Souza
 * -----
 * Description:
 * ············
 * *****
 */

package fundamentos;

import java.util.Scanner;
import java.util.Locale;

public class DesafioConversao {
    public static void main(String[] args) {
        // Locale.setDefault(new Locale("en", "US")); // deprecated // Força o uso de ponto ao invés de vírgula (java.util.Locale)
        @SuppressWarnings("resource")
        Scanner entrada = new Scanner(System.in);
        Double soma = 0.0;
        for (int i = 0; i < 3; i++) {
            System.out.print("Digite o " + (i + 1) + "º salário: ");
            String salario = entrada.nextLine();
            System.out.println("Salário " + (i + 1) + ": " + salario);
            soma += (Double.parseDouble(salario.replace(",", ".")));
        }
        System.out.println("Soma dos salários: " + soma);
        System.out.println("Média dos salários: " + soma / 3);
    }
}