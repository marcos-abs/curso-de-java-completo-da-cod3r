/*
 * *****
 * File: ConversaoTipoPrimitivoNumerico.java
 * Project: Curso de Java Completo da Cod3r
 * Path: /home/marcos/.path-shortcuts/curso-java-cod3r
 * File Created: Monday, 20 March 2023 14:10:22
 * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 05 June 2023 14:39:49
 * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Copyright (c) 2023 All rights reserved, Marcos Antônio Barbosa de Souza
 * -----
 * Description:
 * ············
 * https://www.udemy.com/course/fundamentos-de-programacao-com-java/learn/
 * lecture/15397206#overview
 * *****
 */
package fundamentos;

public class ConversaoTipoPrimitivoNumerico {
    public static void main(String[] args) {
        double a = 1; // int para double (implicita - sem perda de informação)
        System.out.println(a);

        float b = (float) 1.123456789; // double para float (explícita - com perda de informação)
        System.out.println(b);

        int c = 340;
        byte d = (byte) c; // int para byte (explícita - com perda de informação - 340 > 127)
        System.out.println(d);

        double e = 1.9999999;
        int f = (int) e; // double para int (explícita - sem perda de informação - 1.9999999 > 1)
        System.out.println(f);
    }
}