/*
 * *****
 * File: Wrapper.java
 * Project: Curso de Java Completo da Cod3r
 * Path: /home/marcos/.path-shortcuts/curso-java-cod3r
 * File Created: Monday, 20 March 2023 13:09:10
 * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 20 March 2023 13:24:28
 * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Copyright (c) 2023 All rights reserved, Marcos Antônio Barbosa de Souza
 * -----
 * Description:
 * ············ https://www.udemy.com/course/fundamentos-de-programacao-com-java/learn/lecture/15397200#overview
 * *****
 */
package fundamentos;

import java.util.Scanner;

public class Wrapper { // Wrapper é a versão objeto dos tipos primitivos (wrapper == embrulho, pacote,
                       // etc)
    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        // byte
        Byte b = 100; // Byte != byte (byte é um tipo primitivo e Byte é uma classe)
        Short s = 1000; // Short != short (short é um tipo primitivo e Short é uma classe)
        // Integer i = 10000; // int
        System.out.println("Digite um número inteiro: ");
        Integer i = Integer.parseInt(entrada.next()); // Integer != int (int é um tipo primitivo e
                                                      // Integer é uma classe)
        Long l = 100000L; // observe o "L" no final (Long != long (long é um tipo primitivo e
                          // Long é uma classe))

        System.out.println(b.byteValue());
        System.out.println(s.toString());
        System.out.println(i * 3);
        System.out.println(l / 3);

        Float f = 123.10F; // observe o "F" no final (Float != float (float é um tipo primitivo e
                           // Float é uma classe))
        System.out.println(f);

        Double d = 1234.5678; // Double != double (double é um tipo primitivo e Double é uma
                              // classe)
        System.out.println(d);

        Boolean bo = Boolean.parseBoolean("true"); // Boolean != boolean (boolean é um tipo primitivo e
                                                   // Boolean é uma classe)
        System.out.println(bo);
        System.out.println(bo.toString().toUpperCase());

        Character c = '#'; // Character != char (char é um tipo primitivo e Character é uma classe)
        System.out.println(c + "...");
        entrada.close();
    }
}