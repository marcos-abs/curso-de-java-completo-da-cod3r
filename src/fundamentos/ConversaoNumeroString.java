/*
 * *****
 * File: ConversaoNumeroString.java
 * Project: Curso de Java Completo da Cod3r
 * Path: /home/marcos/.path-shortcuts/curso-java-cod3r
 * File Created: Monday, 05 June 2023 14:43:32
 * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 05 June 2023 15:03:29
 * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Copyright (c) 2023 All rights reserved, Marcos Antônio Barbosa de Souza
 * -----
 * Description: Aula 36. Conversão Número -> String
 * ············
 * *****
 */
package fundamentos;

public class ConversaoNumeroString {
    public static void main(String[] args) {
        Integer num1 = 10000;
        Short num3 = 30000;
        Float num4 = 40000F;
        Double num5 = 50000.0;
        System.out.println("Tamanho num1(int): " + num1.toString().length());

        int num2 = 100000;
        System.out.println("Tamanho num2(Integer): " + Integer.toString(num2).length());
        System.out.println("Tamanho num3(Short): " + Short.toString(num3).length());
        System.out.println("Tamanho num4(Float): " + Float.toString(num4).length());
        System.out.println("Tamanho num5(Double): " + Double.toString(num5).length());

        System.out.println("Tamanho num1(int + string): " + (" " + num1).length());
        System.out.println("Tamanho num2(Integer + string): " + (" " + num2).length());
        System.out.println("Tamanho num3(Short + string): " + (" " + num3).length());
        System.out.println("Tamanho num4(Float + string): " + (" " + num4).length());
        System.out.println("Tamanho num5(Double + string): " + (" " + num5).length());
    }
}