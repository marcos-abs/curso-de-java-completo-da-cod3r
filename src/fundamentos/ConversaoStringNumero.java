/*
 * *****
 * File: ConversaoStringNumero.java
 * Project: Curso de Java Completo da Cod3r
 * Path: /home/marcos/.path-shortcuts/curso-java-cod3r
 * File Created: Monday, 05 June 2023 15:07:18
 * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 05 June 2023 15:39:01
 * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Copyright (c) 2023 All rights reserved, Marcos Antônio Barbosa de Souza
 * -----
 * Description:
 * ············ Aula 37. Conversão String -> Número
 * *****
 */

package fundamentos;

import javax.swing.JOptionPane;

public class ConversaoStringNumero {
    public static void main(String[] args) {
        String valor1 = JOptionPane.showInputDialog("Digite o primeiro número: ");
        String valor2 = JOptionPane.showInputDialog("Digite o segundo número: ");
        System.out.println(valor1 + valor2);
        double numero1 = Double.parseDouble(valor1);
        double numero2 = Double.parseDouble(valor2);
        double soma = numero1 + numero2;

        System.out.println("Soma é " + soma);
        System.out.println("Média é " + soma / 2);
    }
}