/*
 * *****
 * File: PrimitivoVsObjeto.java
 * Project: Curso de Java Completo da Cod3r
 * Path: /home/marcos/.path-shortcuts/curso-java-cod3r
 * File Created: Monday, 20 March 2023 13:03:49
 * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 20 March 2023 13:31:49
 * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
 * -----
 * Copyright (c) 2023 All rights reserved, Marcos Antônio Barbosa de Souza
 * -----
 * Description:
 * ············ https://www.udemy.com/course/fundamentos-de-programacao-com-java/learn/lecture/15397198?start=0#overview
 * *****
 */
package fundamentos;

public class PrimitivoVsObjeto {
    public static void main(String[] args) {
        String s = new String("texto");
        s.toUpperCase();
        System.out.println(s);

        // Wrappers são a versão objeto dos tipos primitivos
        int a = 123;
        System.out.println(a);
    }
}